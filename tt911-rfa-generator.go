package main

import (
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"regexp"
	"strings"
	"time"
)

// Config represents configuration struct
type Config struct {
	Depth                    int
	MinRfaPerDay             int
	MaxRfaPerDay             int
	MaxCallsPerYearPerCaller int
	HomeAreaCode             int
	GguestAreaCode           int
	HomeCallsRatio           float32
	CallTakers               []string
	Callers                  []string
	Conversations            [][]string
}

var cdrfaFile *os.File
var cdaliFile *os.File
var cdtxtmsgFile *os.File
var cdrfalogFile *os.File
var config Config
var phoneNumbers = make(map[int]int)
var rfaSeqNumber int

type coordinates struct {
	lattitude float32
	longitude float32
}

func newTexter(isLocal bool) string {
	area := config.GguestAreaCode
	if isLocal {
		area = config.HomeAreaCode
	}
	var phone = fmt.Sprintf("%03d%07d", area, phoneNumbers[area])
	phoneNumbers[area]++

	return phone
}

func getUUID() string {
	u := make([]byte, 16)
	_, err := rand.Read(u)
	if err != nil {
		return ""
	}

	u[8] = (u[8] | 0x80) & 0xBF // what does this do?
	u[6] = (u[6] | 0x40) & 0x4F // what does this do?

	return hex.EncodeToString(u)
}

func addLogRecord(status rune, rfaID string, callTaker string, date time.Time) {
	fmt.Fprintf(cdrfalogFile, "%s|%-263s|%c|%-15s|%s\n", getUUID(), rfaID, status, callTaker, date.Format("15:04:05 01/02/2006"))
}

func generateRfaRecords(date time.Time, phone string) {
	rfaID := fmt.Sprintf("%s-%010d", phone, rfaSeqNumber)
	rfaSeqNumber++
	callTaker := config.CallTakers[rand.Intn(len(config.CallTakers))]

	// Write to cdrfa
	prettyPhone := fmt.Sprintf("(%s)%s-%s", phone[0:3], phone[3:6], phone[6:10])
	caller := config.Callers[rand.Intn(len(config.Callers))]
	fmt.Fprintf(cdrfaFile, "%-263s|T|%9s|%-18s|%-120s|%1s\n", rfaID, "", prettyPhone, caller, "M")

	// Write to cdtxtmsg
	conversation := config.Conversations[rand.Intn(len(config.Conversations))]
	initCoordRegexp := regexp.MustCompile("^SYSTEM: Approximate Cell Sector location provided by network:latitude, longitude = (-?[0-9]+\\.[0-9]{3}[0-9]*),\\s+(-?[0-9]+\\.[0-9]{3}[0-9]*)")
	interCoordRegexp := regexp.MustCompile("^SYSTEM: updated location: latitude, longitude, radius, timestamp = (-?[0-9]+\\.[0-9]{3}[0-9]*),\\s+(-?[0-9]+\\.[0-9]{3}[0-9]*),\\s+([0-9]+),")
	transcriptNumber := 1
	presented := false
	accepted := false
	for _, c := range conversation {
		strings.Replace(c, "{date}", date.UTC().Format("Mon Jan 2 15:04:05 MST 2006"), -1)
		if c[0] == '>' { // incoming message
			fmt.Fprintf(cdtxtmsgFile, "%-263s|%s|%-4d|%-15s|%s\n", rfaID, date.Format("15:04:05 01/02/2006"),
				transcriptNumber, "", c[1:])
			if !presented {
				presented = true
				addLogRecord('P', rfaID, callTaker, date)
			}
		} else {
			fmt.Fprintf(cdtxtmsgFile, "%-263s|%s|%-4d|%-15s|%s\n", rfaID, date.Format("15:04:05 01/02/2006"),
				transcriptNumber, callTaker, c[1:])
			if !accepted {
				accepted = true
				addLogRecord('A', rfaID, callTaker, date)
			}
		}

		// Write to cdali
		coord := initCoordRegexp.FindStringSubmatch(c[1:])
		if len(coord) == 3 {
			//log.Fatalf("[%s], [%s], [%s]\n", coord[0], coord[1], coord[2])
			fmt.Fprintf(cdaliFile, "%s|%-263s|%s|%12s|%12s|%4d|\n", getUUID(), rfaID,
				date.Format("15:04:05 01/02/2006"), coord[1], coord[2], 0)
		} else {
			coord = interCoordRegexp.FindStringSubmatch(c[1:])
			if len(coord) == 4 {
				//log.Fatalf("[%s], [%s], [%s], [%s]\n", coord[0], coord[1], coord[2], coord[3])
				fmt.Fprintf(cdaliFile, "%s|%-263s|%s|%12s|%12s|%4s|\n", getUUID(), rfaID,
					date.Format("15:04:05 01/02/2006"), coord[1], coord[2], coord[3])
			}
		}

		transcriptNumber++
		date = date.Add(time.Duration(rand.Intn(10)+1) * time.Second)
	}
	addLogRecord('T', rfaID, callTaker, date)
}

func openFiles() {
	var err error

	cdrfaFile, err = os.Create("cdrfa.txt")
	if err == nil {
		cdrfaFile.WriteString("# cdrfa|rfaid|status|callid|phone|caller|hwrcvd\n")
		cdaliFile, err = os.Create("cdali.txt")
		if err == nil {
			cdaliFile.WriteString("# cdrfalog|uuid|rfaid|status|who|time\n")
			cdrfalogFile, err = os.Create("cdrfalog.txt")
			if err == nil {
				cdrfalogFile.WriteString("# cdrfalog|uuid|rfaid|act|who|time\n")
				cdtxtmsgFile, err = os.Create("cdtxtmsg.txt")
				if err == nil {
					cdtxtmsgFile.WriteString("# cdtxtmsg|rfaid|rcvd|trid|sender|message\n")
				}
			}
		}
	}

	if err != nil {
		log.Fatal(err)
	}
}

func closeFiles() {
	cdrfaFile.Close()
	cdaliFile.Close()
	cdrfalogFile.Close()
	cdtxtmsgFile.Close()
}

func main() {
	// Initialization
	rand.Seed(time.Now().UnixNano())

	configBytes, err := ioutil.ReadFile("config.json")
	if err == nil {
		err = json.Unmarshal(configBytes, &config)
	}
	if err != nil {
		log.Fatal(err)
	}
	neededCallsCount := config.Depth * config.MaxRfaPerDay
	endTime := time.Now()
	calls := make([]string, 0, neededCallsCount)

	// Generation of calls slice
CallsGeneration:
	for {
		isLocal := rand.Float32() < config.HomeCallsRatio
		t := newTexter(isLocal)

		var texterCallCount int
		texterCallCount = rand.Intn(config.MaxCallsPerYearPerCaller) + 1
		for i := 0; i <= texterCallCount; i++ {
			calls = append(calls, t)
			if len(calls) == neededCallsCount {
				break CallsGeneration
			}
		}
	}

	// Shuffle
	for i := range calls {
		j := rand.Intn(i + 1)
		calls[i], calls[j] = calls[j], calls[i]
	}

	// RFA generation
	fmt.Printf("Generating about %d RFAs\n", neededCallsCount)
	openFiles()
	for currentDepth := config.Depth; currentDepth >= 0; currentDepth-- {
		date := endTime.AddDate(0, 0, -currentDepth)
		rfasThisDay := rand.Intn(config.MaxRfaPerDay-config.MinRfaPerDay) + config.MinRfaPerDay
		for i := 0; i < rfasThisDay; i++ {
			callDate := date.Add(time.Second * time.Duration(rand.Int63n(3600*24)))
			call := calls[len(calls)-1]
			calls = calls[:len(calls)-1]
			generateRfaRecords(callDate, call)
		}
	}
	closeFiles()
	fmt.Println("Done.")
}
